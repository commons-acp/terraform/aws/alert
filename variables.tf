variable "log_group_name" {
  description = "The log group name that is going to be analysed"
}
variable "alert_pattern" {
  default = "status 500"
  description = "The pattern of the filter"
}
variable "alert_name" {
  description = "the filter name"
}
variable "alarm_period" {
  description = "The alarm period"
  default = "10"
}
variable "subscriptions" {
  description = "List of subscriptions that need to be integrated"
  type = map(object({
    endpoint = string
  }))
  default = {}
}