

resource "aws_cloudwatch_log_metric_filter" "filter" {
  name = "${var.alert_name}-filter"
  pattern = var.alert_pattern
  log_group_name = var.log_group_name

  metric_transformation {
    name = local.metric_name
    namespace = local.metric_ns
    value = 1
  }
}
locals {
  metric_name = "${var.alert_name}-filter"
  metric_ns = "${var.alert_name}-ns"
}
resource "aws_sns_topic" "topic_errors" {
  name = "errros"
}
resource "aws_cloudwatch_metric_alarm" "webapp_timeout" {
  alarm_name                = var.alert_name
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  metric_name               = local.metric_name
  namespace                 = local.metric_ns
  period                    = var.alarm_period
  statistic                 = "SampleCount"
  threshold                 = "1"
  alarm_description         = "This metric monitors ${var.alert_name}"
  insufficient_data_actions = []
  ok_actions = []
  alarm_actions             = [aws_sns_topic.topic_errors.arn]
}
resource "aws_sns_topic_subscription" "sns-topic" {
  for_each = var.subscriptions
  topic_arn = aws_sns_topic.topic_errors.arn
  protocol  = "email"
  endpoint = each.value.endpoint
}
